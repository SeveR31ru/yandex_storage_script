import os
import boto3

BUCKET_NAME = "your-bucket-name"


def list_of_bucket_objects(bucket_name, client):
    try:
        for key in client.list_objects_v2(Bucket=bucket_name)["Contents"]:
            print(key["Key"])
    except KeyError:
        print("Bucket is empty")


def list_of_files_in_bucket_folder(bucket_name, folder_path, client):
    try:
        for key in client.list_objects_v2(Bucket=bucket_name, Prefix=folder_path)[
            "Contents"
        ]:
            print(key["Key"])
    except KeyError:
        print("Folder is empty")


def add_folder_to_s3(bucket_name, client, folder_path):
    for root, dirs, files in os.walk(folder_path):
        for file_name in files:
            # Construct the full local path to the file
            local_file_path = os.path.join(root, file_name)

            # Construct the S3 key (object key) using the relative path of the file
            s3_key = os.path.relpath(local_file_path, folder_path)

            # Upload the file to S3
            client.upload_file(local_file_path, bucket_name, s3_key,ExtraArgs={'ACL':'public-read'})

            print(
                f"{local_file_path} uploaded successfully to {bucket_name} with key {s3_key}."
            )


def delete_all_files_from_bucket_folder(bucket_name, client, folder_path):
    obj_list = client.list_objects_v2(Bucket=bucket_name, Prefix=folder_path)
    files_in_folder = obj_list["Contents"]
    files_to_delete = []
    for f in files_in_folder:
        files_to_delete.append({"Key": f["Key"]})
        print(f"File {f['Key']} will be deleted")
    # This will delete all files in a folder
    client.delete_objects(Bucket=bucket_name, Delete={"Objects": files_to_delete})


session = boto3.session.Session()
client = session.client(
    service_name="s3", endpoint_url="https://storage.yandexcloud.net"
)

list_of_bucket_objects(BUCKET_NAME,client)
